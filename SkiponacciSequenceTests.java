import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SkiponacciSequenceTests {
    // Test the three fields (n<3)
    @Test
    public void getTermGetter1(){
        SkiponacciSequence ss1 = new SkiponacciSequence(8,20,5);
        assertEquals(8, ss1.getTerm(0));
    } 
    @Test
    public void getTermGetter2(){
        SkiponacciSequence ss1 = new SkiponacciSequence(3,1,6);
        assertEquals(1, ss1.getTerm(1));
    } 
    @Test
    public void getTermGetter3(){
        SkiponacciSequence ss1 = new SkiponacciSequence(10,15,1);
        assertEquals(1, ss1.getTerm(2));
    } 
    // Tests iteration (n>3)
    @Test
    public void getTermTest1(){
        SkiponacciSequence ss1 = new SkiponacciSequence(1, 2, 3);
        assertEquals(3, ss1.getTerm(3));
    }
    @Test
    public void getTermTest2(){
        SkiponacciSequence ss1 = new SkiponacciSequence(2,4,1);
        assertEquals(6, ss1.getTerm(3));
    }
}
