public class SkiponacciSequence extends Sequence {
    // Constructor method and fields
    private int a;
    private int b;
    private int c;

    public SkiponacciSequence(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    // Gets the nth number in the sequence
    @Override
    public int getTerm(int n){
        if(n<3){
            return getTermUnderThree(n);
        }
        else{
            return getTermOverThree(n);
        }
    }
    // Find a term where n<3 by returning a,b or c directly
    private int getTermUnderThree(int n){
        if(n ==0){
            return a;
        }
        else if(n==1){
            return b;
        }
        else{
            return c;
        } 
    }
    // Find the term is found if n>3
    private int getTermOverThree(int n){
        int iterations = n-2; // remove iterations 0 to 2 (above test)
        int a = this.a;
        int b = this.b;
        int c = this.c;
        for(int i = 0; i<iterations; i++){
            int next = a+b;
            a = b;
            b = c;
            c = next;
        }
        return c;
    }

}
