import java.util.Scanner;

public class MainMethod {
    static final int SEQUENCE_ITEM_COUNT = 4;
    public static void main(String[]args){
        // Get the scanner and read from it
        final int SEQUENCE_ITERATIONS = 10; 
        System.out.println("Input sequences:");
        Scanner scanner = new Scanner(System.in);
        String sequenceString = scanner.next();
        // Get the sequences by parsing the scanner input
        Sequence[] sequences = parse(sequenceString);
        print(sequences, SEQUENCE_ITERATIONS);
    }
    public static void print(Sequence[] sequences, int n){
        for (Sequence i : sequences){
            System.out.println(i.getTerm(n));
        }
    }   
    // Parse the sequenceString into an array of Sequences
    public static Sequence[] parse(String sequenceString){
        int[][] sequenceData = getSequenceData(sequenceString); // you need to get all the ints in one place
        Sequence[] sequences = new Sequence[sequenceData.length];
        // loop through sequence data
        for(int i = 0; i < sequenceData.length; i++){
            int[] argumentData = sequenceData[i];
            boolean isFibonacci = argumentData.length == 2; 
            // create fibonacci or skiponacci based on number of args
            if(isFibonacci){
                sequences[i] = new FibonacciSequence(argumentData[0],argumentData[1]);
            }
            else{
                sequences[i] = new SkiponacciSequence(argumentData[0],argumentData[1],argumentData[2]);
            }                
        } 
        return sequences;
    }
    // Parses the sequence string into a 2d array
    public static int[][] getSequenceData(String sequenceString){
        // First split the string and get the count for teh sequence
        String[] splitData = sequenceString.split(";");
        int sequenceCount = getSequenceCount(splitData);
        // Create an array for the sequence data. 
        int[][] sequenceData = new int[sequenceCount][];

        // Loop through the split data to parse it
        for(int i = 0; i<splitData.length; i++){
            String data = splitData[i];
            int sequenceIndex = i/SEQUENCE_ITEM_COUNT; // Integer division tells us which sequence we're at
            int argumentData = (i)%SEQUENCE_ITEM_COUNT-1; // -1 to start param index at 0 
            if(isInteger(data)){
                sequenceData[sequenceIndex][argumentData] = Integer.parseInt(data);
            } 
            // init second array here since fibonacci and skiponacci have different constructor counts
            else if(data.equals("fib")||data.equals("skip")){
                int numberParams = data.equals("fib")? 2 : 3; // get the number of parameters for the constructor
                sequenceData[sequenceIndex] = new int[numberParams];
            }
        }
        return sequenceData;
    }
    // Helper method to check if a string is an integer 
    //(mainly to avoid using a try catch statement where i'm not supposed to) 
    public static boolean isInteger(String string){
        try {
            Integer.parseInt(string);
            
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    } 
    // Returns the amount of sequences in a splitted sequence string
    public static int getSequenceCount(String[] splitData)
    {
        return splitData.length/SEQUENCE_ITEM_COUNT;
    }
}
