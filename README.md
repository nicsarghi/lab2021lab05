# lab2021lab05

## git lab url
https://gitlab.com/nicsarghi/lab2021lab05/

## collaborators 
- Nicoleta Sarghi
- Kelsey Pereira Costa

## task distribution
- FibonacciSequence -> Kelsey 
- SkiponacciSequenceTests -> Kelsey 
- SkiponacciSequence -> Nicoleta
- TestFibonacci -> Nicoleta  
- print method in MainMethod -> Kelsey
- parse method in MainMethod -> Nicoleta

