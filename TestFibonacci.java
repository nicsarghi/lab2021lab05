import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestFibonacci {
    @Test
    public void TestTermGetter0(){
        int expected = 3;
        FibonacciSequence sequence = new FibonacciSequence(3,1);
        assertEquals(expected,sequence.getTerm(0));        
    }

    @Test
    public void TestTermGetter1(){        
        int expected = 7;
        FibonacciSequence sequence = new FibonacciSequence(4,7);
        assertEquals(expected,sequence.getTerm(1));
    }

    @Test 
    public void TestTermGetterA(){
        int expected = 275;
        FibonacciSequence sequence = new FibonacciSequence(5,10);
        assertEquals(expected,sequence.getTerm(8));
    }
    @Test 
    public void TestTermGetterB(){
        int expected = 16;
        FibonacciSequence sequence = new FibonacciSequence(2,4);
        assertEquals(expected,sequence.getTerm(4));
    }
    @Test 
    public void TestTermGetterC(){
        int expected = 22;
        FibonacciSequence sequence = new FibonacciSequence(4,2);
        assertEquals(expected,sequence.getTerm(5));
    }
}
