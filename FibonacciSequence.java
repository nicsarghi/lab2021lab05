public class FibonacciSequence extends Sequence {
    private int a;
    private int b;
    public FibonacciSequence(int a, int b){
        this.a = a;
        this.b = b;
    }
    //gets the n-th number in the sequence 
    @Override
    public int getTerm(int n){
        // return a or b if its the first two fields
        if(n==0){
            return this.a;
        }
        else if(n==1){
            return this.b;
        } 
        // else iterate
        int a = this.a;
        int b = this.b;
        int nthNum = 0;
        for (int i = 0; i < n-1; i++){
            nthNum = a + b;
            a = b;
            b = nthNum;
        }
        return nthNum;
    }
}
